package com.example.CAMEBOL.usuarios.models.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.CAMEBOL.usuarios.models.entity.Rol;
import com.example.CAMEBOL.usuarios.models.entity.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {

	@Query("select u from Usuario u where u.nombreUsuario = ?1 and u.password = ?2")
	public Usuario Login(String nom, String password);
	
}
