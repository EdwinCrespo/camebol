package com.example.CAMEBOL.usuarios.models.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "Rol")
public class Rol {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idRol;
	private String nombre;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRegistro;
	private Date fechaActualizacion;
	private Short estado;
	
	// @JsonIgnoreProperties(value = {"rol"}, allowSetters = true)
	//@OneToMany(mappedBy = "rol", fetch = FetchType.LAZY, cascade =CascadeType.ALL, orphanRemoval = true )
	//private List<Usuario> usuarios;
	

	
	
	//public List<Usuario> getUsuarios() {
	//	return usuarios;
	//}


	//public void setUsuarios(List<Usuario> usuarios) {
	//	this.usuarios.clear();
	//	usuarios.forEach(this::addUsuarios);
		
	//}
	
	//public Rol() {
	//	this.usuarios=new ArrayList<>();
	//}

	@PrePersist
	public void prePersist()
	{
		this.fechaRegistro=new Date();
	}
	
	public Integer getIdRol() {
		return idRol;
	}
	public void setIdRol(Integer idRol) {
		this.idRol = idRol;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}
	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}
	public Short getEstado() {
		return estado;
	}
	public void setEstado(Short estado) {
		this.estado = estado;
	}
	
	//public void addUsuarios(Usuario usuarios) {
	//	this.usuarios.add(usuarios);
	//	usuarios.setRol(this);
	//}

	//public void removeUsuarios(Usuario usuarios) {
	//	this.usuarios.remove(usuarios);
	//	usuarios.setRol(null);
	//}
	
	
}
