package com.example.CAMEBOL.usuarios.controllers;

import java.util.Date;
import java.util.Optional;

import javax.persistence.MappedSuperclass;
import javax.ws.rs.Path;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.CAMEBOL.CRUD.controllers.CommonController;
import com.example.CAMEBOL.usuarios.models.entity.Rol;
import com.example.CAMEBOL.usuarios.services.RolServices;

@RestController
@RequestMapping("/rol")
public class RolController extends CommonController<Rol, RolServices>  {

	@PutMapping("/{id}")
	public ResponseEntity<?> editar(@RequestBody Rol rol,@PathVariable Integer id)
	{
		Optional<Rol> o = service.findById(id);
		
		if (o.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		Rol roldb= o.get();
		roldb.setNombre(rol.getNombre());
		roldb.setEstado(rol.getEstado());
		roldb.setFechaActualizacion(new Date());
		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(roldb));
	}
}
