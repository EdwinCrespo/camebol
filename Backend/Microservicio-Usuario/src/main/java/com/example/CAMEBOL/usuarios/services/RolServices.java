package com.example.CAMEBOL.usuarios.services;

import com.example.CAMEBOL.CRUD.services.CommonService;
import com.example.CAMEBOL.usuarios.models.entity.Rol;

public interface RolServices extends CommonService<Rol> {

}
