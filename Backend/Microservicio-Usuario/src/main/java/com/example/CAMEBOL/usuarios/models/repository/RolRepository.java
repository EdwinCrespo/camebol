package com.example.CAMEBOL.usuarios.models.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.CAMEBOL.usuarios.models.entity.Rol;

public interface RolRepository extends CrudRepository<Rol, Integer> {


}
