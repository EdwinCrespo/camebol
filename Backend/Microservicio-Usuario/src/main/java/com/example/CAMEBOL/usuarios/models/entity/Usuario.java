package com.example.CAMEBOL.usuarios.models.entity;

import java.sql.Blob;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.type.descriptor.sql.SmallIntTypeDescriptor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "Usuario")
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idUsuario;
	
	private String nombre;
	private String primerApellido;
	private String segundoApellido;
	private Date fechaNacimiento;
	private Byte imagen;
	
	private String email;
	private String numero;
	private String nombreUsuario;
	private String password;

	//@JsonIgnoreProperties(value = {"usuario"})
	//@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="rolId")
	private Integer rolId;
	private Integer idOrganizacion;

	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRegistro;
	private Date fechaActualizacion;
	private Short estado;
	
	
	@PrePersist
	public void prePersist()
	{
		this.fechaRegistro=new Date();
	}


		
	//Getters and Setters
	public Integer getIdUsuario() {
		return idUsuario;
	}


	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getPrimerApellido() {
		return primerApellido;
	}


	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}


	public String getSegundoApellido() {
		return segundoApellido;
	}


	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}


	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}


	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}


	public Byte getImagen() {
		return imagen;
	}


	public void setImagen(Byte imagen) {
		this.imagen = imagen;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getNumero() {
		return numero;
	}


	public void setNumero(String numero) {
		this.numero = numero;
	}


	public String getNombreUsuario() {
		return nombreUsuario;
	}


	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}
	
	public Integer getRolId() {
		return rolId;
	}



	public void setRolId(Integer rolId) {
		this.rolId = rolId;
	}



	public Integer getIdOrganizacion() {
		return idOrganizacion;
	}


	public void setIdOrganizacion(Integer idOrganizacion) {
		this.idOrganizacion = idOrganizacion;
	}


	public Date getFechaRegistro() {
		return fechaRegistro;
	}


	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}


	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}


	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}


	public Short getEstado() {
		return estado;
	}


	public void setEstado(Short estado) {
		this.estado = estado;
	}



	@Override
	public boolean equals(Object obj) {
		if(this==obj)
		{
			return true;
		}
		if(!(obj instanceof Usuario))
		{
			return false;
		}
		Usuario u=(Usuario) obj;
		
		
		
		return this.idUsuario!=null && this.idUsuario.equals(u.getIdUsuario());
	}
	
	
	

}
