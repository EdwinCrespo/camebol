package com.example.CAMEBOL.usuarios.controllers;

import java.util.Date;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.CAMEBOL.CRUD.controllers.CommonController;
import com.example.CAMEBOL.usuarios.models.entity.Rol;
import com.example.CAMEBOL.usuarios.models.entity.Usuario;
import com.example.CAMEBOL.usuarios.services.UsuarioServices;

@RestController
public class UsuarioController extends CommonController<Usuario, UsuarioServices> {

	@PutMapping("/{id}")
	public ResponseEntity<?> editar(@RequestBody Usuario usuario,@PathVariable Integer id)
	{
		Optional<Usuario> o = service.findById(id);
		
		if (o.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		Usuario usuarioDb= o.get();
		usuarioDb.setNombre(usuario.getNombre());
		usuarioDb.setPrimerApellido(usuario.getPrimerApellido());
		usuarioDb.setSegundoApellido(usuario.getSegundoApellido());
		usuarioDb.setFechaNacimiento(usuario.getFechaNacimiento());
		usuarioDb.setImagen(usuario.getImagen());
		usuarioDb.setEmail(usuario.getEmail());
		usuarioDb.setNumero(usuario.getNumero());
		usuarioDb.setNombreUsuario(usuario.getNombreUsuario());
		usuarioDb.setPassword(usuario.getPassword());
		usuarioDb.setRolId(usuario.getRolId());
		usuarioDb.setIdOrganizacion(usuario.getIdOrganizacion());
		usuarioDb.setFechaActualizacion(new Date());
		usuarioDb.setEstado(usuario.getEstado());
		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(usuarioDb));
	}
	
	
	
	
	@GetMapping("/login/{nom}/{password}")
	public ResponseEntity<?> filtrar(@PathVariable String nom,@PathVariable String password)
	{
		return ResponseEntity.ok(service.Login(nom, password));
	}
}
