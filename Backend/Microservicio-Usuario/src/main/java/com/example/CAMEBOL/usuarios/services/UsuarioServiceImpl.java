package com.example.CAMEBOL.usuarios.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.example.CAMEBOL.CRUD.services.CommonServiceImpl;
import com.example.CAMEBOL.usuarios.models.entity.Rol;
import com.example.CAMEBOL.usuarios.models.entity.Usuario;
import com.example.CAMEBOL.usuarios.models.repository.UsuarioRepository;
@Service
public class UsuarioServiceImpl extends CommonServiceImpl<Usuario,UsuarioRepository> implements UsuarioServices{
	@Override
	@Transactional
	public Usuario Login(String nom, String password)
	{
		return repository.Login(nom, password);
	}
}
