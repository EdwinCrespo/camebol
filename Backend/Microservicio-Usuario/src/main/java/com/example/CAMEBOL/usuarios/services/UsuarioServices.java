package com.example.CAMEBOL.usuarios.services;

import java.util.List;

import com.example.CAMEBOL.CRUD.services.CommonService;
import com.example.CAMEBOL.usuarios.models.entity.Rol;
import com.example.CAMEBOL.usuarios.models.entity.Usuario;

public interface UsuarioServices extends CommonService<Usuario>{

	public Usuario Login(String nom, String password);
}
