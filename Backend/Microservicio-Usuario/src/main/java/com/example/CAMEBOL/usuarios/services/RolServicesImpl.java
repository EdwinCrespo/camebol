package com.example.CAMEBOL.usuarios.services;

import org.springframework.stereotype.Service;

import com.example.CAMEBOL.CRUD.services.CommonServiceImpl;
import com.example.CAMEBOL.usuarios.models.entity.Rol;
import com.example.CAMEBOL.usuarios.models.repository.RolRepository;

@Service
public class RolServicesImpl extends CommonServiceImpl<Rol,RolRepository> implements RolServices {

}
