package com.example.CAMEBOL.CRUD.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.CAMEBOL.CRUD.services.CommonService;

public class CommonController <E, S extends CommonService<E>>{

	@Autowired
	protected S service;
	
	@GetMapping // es para declarar una ruta url y si no especificamos es porque esta en la raiz
	public ResponseEntity<?> listar(){
		
		return ResponseEntity.ok().body(service.findAll());
	}
	
	@GetMapping("/{id}") // pasamos un parametro
	public ResponseEntity<?> ver(@PathVariable Integer id){
		
		Optional<E> o = service.findById(id);
		
		if (o.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok().body(o.get());
		
	}
	
	@PostMapping
	public ResponseEntity<?> crear(@RequestBody E entity)
	{
		E entityDb = service.save(entity);
		return ResponseEntity.status(HttpStatus.CREATED).body(entityDb);
	}

	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> eliminar(@PathVariable Integer id)
	{
		service.deleteById(id);
		
		return ResponseEntity.noContent().build();
	}
	
}
