package com.example.CAMEBOL.CRUD.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

public class CommonServiceImpl <E, R extends CrudRepository<E,Integer>> implements CommonService<E>  {

	@Autowired
	protected R repository;
	
	@Override
	@Transactional(readOnly = true)// solo lectura
	public Iterable<E> findAll() {
		
		return repository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<E> findById(Integer id) {
		
		return repository.findById(id);
	}

	@Override
	@Transactional // para la escritura
	public E save(E entity) {
	
		return repository.save(entity);
	}

	@Override
	@Transactional
	public void deleteById(Integer id) {
		
		repository.deleteById(id);
		
	}


}
