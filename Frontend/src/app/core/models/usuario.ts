export class Usuario {
  idUsuario: number = 0;
  nombre: string = '';
  primerApellido: string = '';
  segundoApellido: string = '';
  fechaNacimiento: number = Date.now();
  imagen: number = 0;
  email: string = '';
  numero: string = '';
  nombreUsuario: string = '';
  password: string = '';
  rolId: number = 0;
  idOrganizacion: number = 0;
  fechaRegistro: number = Date.now();
  fechaActualizacion: number = Date.now();
  estado: number = 0;
  // constructor(idUsuario:number,nombre: string, apellido: string, apellidoM: string, fechaNacimiento: number, CI: string, numeroTelefono: string, nombreUsuario: string, contrasenia: string, idRol: number){
  // this.idUsuario=idUsuario
  // this.nombre = nombre;
  // this.primerApellido = apellido;
  // this.segundoApellido = apellidoM;
  // this.numero = numeroTelefono;
  // this.fechaNacimiento = fechaNacimiento;
  // this.nombreUsuario = nombreUsuario;
  // this.password = contrasenia;
  // this.rolId = idRol;
  // }
  constructor() {
  }
}