import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { CommonModule } from '@angular/common';

import { NotFoundComponent } from './components/not-found/not-found.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';

@NgModule({
	imports: [
		RouterModule,
		FormsModule,
		MatDialogModule,
		CommonModule
	],

	declarations: [
		NotFoundComponent,
		NavbarComponent,
		FooterComponent
	],

	exports: [
		RouterModule,
		FormsModule,
		MatDialogModule,
		CommonModule,
		NotFoundComponent,
		NavbarComponent,
		FooterComponent
	],
	providers: []
})

export class SharedModule {
	constructor() { }
}