import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CrearUsuarioComponent } from '../../crear-usuario/container/crear-usuario.component';
import { UsuarioService } from '../../../../services/usuario.service';
import { Usuario } from 'src/app/core/models/usuario';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  //verificar = 'algo'
  usuario?: Usuario;
  pass: string = "";
  userName: string = "";
  error: string = "none";
  // contructor para inyectar servicios
  constructor(public dialog: MatDialog, private service: UsuarioService, private route: Router) { }
  // ngOnInit para inicializar los datos del backend que queremos mostrar
  ngOnInit(): void {

  }

  verificar(): void {
    this.service.login(this.userName, this.pass).subscribe(usuario => this.usuario = usuario)
    if (this.usuario != null) {
      this.route.navigate(['/home']);
      this.dialog.closeAll();
    }
    else {
      this.error = 'inline';
    }
  }

  openRegistrarUsuario(): void {
    this.dialog.closeAll();
    const modalRef = this.dialog.open(CrearUsuarioComponent, {
      width: '550px',
    });
    
    modalRef.afterClosed().subscribe(verificar => {
    });
  }

  openRecuperarContrasenia() {
  }

  autenticarUsuario(): void {
    // this.route.navigate(['/home']);
    // this.dialog.closeAll();
    // this.service.login().subscribe(usuario => this.usuario = usuario);
    // if(this.usuario != null) {
    // this.route.navigate(['/home']);
    // this.dialog.closeAll();
    // } else {
    // //Mensaje de error
    // }
    this.verificar();
  }
}
