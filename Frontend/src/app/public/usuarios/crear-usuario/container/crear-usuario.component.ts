import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LoginComponent } from '../../login/container/login.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.component.html',
  styleUrls: ['./crear-usuario.component.css']
})
export class CrearUsuarioComponent implements OnInit {

  constructor(public dialog: MatDialog, private route: Router) { }

  ngOnInit(): void {
  }

  openLogin(): void{
    this.dialog.closeAll();

    const modalRef = this.dialog.open(LoginComponent, {
      width: '280px',
    });

    modalRef.afterClosed().subscribe(verificar => {
      console.log('Modal login');
      console.log(verificar);
    });

  }
}
