import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LoginComponent } from '../../usuarios/login/container/login.component';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
    this.openLogin();
  }

  openLogin(): void{
    const modalRef = this.dialog.open(LoginComponent, {
      width: '280px',
    });

    modalRef.afterClosed().subscribe(verificar => {
      console.log('Modal login');
      console.log(verificar);
    });
  }
}
