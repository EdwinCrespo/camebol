import { NgModule } from '@angular/core';
import { SharedModule } from '../core/shared/shared.module';
import { PublicRoutingModule } from './public-routing.module';

import { PublicComponent } from './public.component';
import { HomeComponent } from './home/container/home.component';
import { InicioComponent } from './inicio/container/inicio.component';
import { QuienesComponent } from './quienes/container/quienes.component';
import { ContactoComponent } from './contacto/container/contacto.component';
import { CrearUsuarioComponent } from './usuarios/crear-usuario/container/crear-usuario.component';
import { LoginComponent } from './usuarios/login/container/login.component';

@NgModule ({
    imports: [
        PublicRoutingModule,
        SharedModule
    ],

    entryComponents:[
        LoginComponent,
        CrearUsuarioComponent
      ],

    declarations: [
        PublicComponent,
        HomeComponent,
        LoginComponent,
        InicioComponent,
        QuienesComponent,
        ContactoComponent,
        CrearUsuarioComponent
    ],

    exports: [],
    providers: []
})

export class PublicModule {
    constructor () {}
}