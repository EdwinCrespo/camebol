import { NgModule } from '@angular/core';
import { SharedModule } from '../core/shared/shared.module';
import { AdminRoutingModule } from './admin-routing.module';

import { AdminComponent } from './admin.component';
import { AdministradorComponent } from './administrador/container/administrador.component';
import { AsideComponent } from './administrador/components/aside/aside.component';
import { MainComponent } from './administrador/components/main/main.component';
import { RightSectionComponent } from './administrador/components/right-section/right-section.component';
import { UserListComponent } from './administrador/components/user-list/user-list.component';
import { EnterprisingListComponent } from './administrador/components/enterprising-list/enterprising-list.component';

@NgModule({
  imports: [
    AdminRoutingModule,
    SharedModule
  ],

  entryComponents: [
  ],

  declarations: [
    AdminComponent,
    AdministradorComponent,
    AsideComponent,
    MainComponent,
    RightSectionComponent,
    UserListComponent,
    EnterprisingListComponent
  ],

  exports: [],
  providers: []
})

export class AdminModule {
  constructor() { }
}