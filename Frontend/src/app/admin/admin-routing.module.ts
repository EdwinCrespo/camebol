import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PermissionsGuard } from "../guard/permissions.guard";

import { AdminComponent } from './admin.component';
import { EnterprisingListComponent } from "./administrador/components/enterprising-list/enterprising-list.component";
import { MainComponent } from "./administrador/components/main/main.component";
import { UserListComponent } from "./administrador/components/user-list/user-list.component";
import { AdministradorComponent } from "./administrador/container/administrador.component";

const routes: Routes = [
	{
		path: '', component: AdminComponent, children: //canActivate: [PermissionsGuard], Sirve para proteger a la ruta
			[
				{ path: '', redirectTo: 'main', pathMatch: 'full' },
				{ path: 'main', component: MainComponent },
				{ path: 'elist', component: EnterprisingListComponent },
				{ path: 'ulist', component: UserListComponent },
				{ path: 'administrador', component: AdministradorComponent }
				
			]
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})

export class AdminRoutingModule {

}