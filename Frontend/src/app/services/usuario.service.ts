import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Usuario } from '../core/models/usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private baseEndpoint = 'http://localhost:8090/api/usuario/login';
  private cabeceras: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })

  constructor(private http: HttpClient) { }

  //Mandamos el login y password y nos retorna el usuario si existe, se puede retornar un booleano, pero por el momento esta asi.
  public login(nombreUsuario: string, contrasenia: string): Observable<Usuario> {
    return this.http.get<Usuario>(`${this.baseEndpoint}/${nombreUsuario}/${contrasenia}`);
  }

  public crear(usuario: Usuario): Observable<Usuario> {
    return this.http.post<Usuario>(this.baseEndpoint, usuario, { headers: this.cabeceras });
  }

}
